#Name: Colman Jin
#ID: 423626
#Class: CSE 330S
#Module 4 

import sys, os, re

#This file reads in a text file with the batting stats of all Cardinals players
#in a certain season. It will then print out all of the batting averages of the 
#players sorted in order of highest batting average to lowest.

#stores the set of all Cardinals players who played during a season
cardsPlayers={}

#checks for valid entries to be calculated within a given string
def checkLine(test):
	#checks for a valid line
	validRegex = "[A-Z][A-Z|a-z]+ [A-Z][A-Z|a-z]+ batted \d+ times with \d+ hits and \d+ runs"
	#checks for name of players
	nameRegex = "[A-Z][A-Z|a-z]+ [A-Z][A-Z|a-z]+"
	#checks for # of bats from a player
	batRegex = "batted \d+"
	#checks for # of hits from a player
	hitRegex = "with \d+"
	
	m = re.match(validRegex, test)
	name = re.search(nameRegex,test)
	bats = re.search(batRegex,test)
	hits = re.search(hitRegex,test)
	#if there are matches
	if not m == None:
		playerName = name.group()
		
		#saves the number
		numBats = test[(bats.span()[0]+7):bats.span()[1]]
		numHits = test[(hits.span()[0]+5):hits.span()[1]]
		
		#checks if the player exists yet, if so update stats and if not, create said player
		if playerName in cardsPlayers.keys():
			p = cardsPlayers.get(playerName)
			p.updateStats(numBats, numHits)
			p.updatePercentage()
		else: 
			p = Player(playerName)
			cardsPlayers[playerName] = p
			p.updateStats(numBats, numHits)
			p.updatePercentage()
			
#given a dictionary of names:players, print out all the entries in sorted batting averages
def printSorted(dict):
	batAvgs = []
	for key in dict:
		batAvgs.append(dict.get(key).percent)
	
	sortedBAvgs=sorted(batAvgs, reverse=True)
	
	sortedPlayers=[]
	temp = ""
	for i in range(len(batAvgs)):
		for key in dict:
			if sortedBAvgs[i] == dict.get(key).percent and dict.get(key).listed == False:
				dict.get(key).listed = True
				temp = key
				break
		
		sortedPlayers.append(temp)
	
	for key in dict:
		dict.get(key).listed = False
	
	for i in range(len(sortedPlayers)):
		print sortedPlayers[i] + " " + dict.get(sortedPlayers[i]).percent

#where each player's information is stored		
class Player:
	#default constructor
	def __init__(self, name):
		self.atBats = 0
		self.hits = 0
		self.percent = 0.0 
		self.name = name
		self.listed = False
	def updateStats(self, gBats, gHits):
		self.atBats=self.atBats+int(gBats)
		self.hits=self.hits+int(gHits)
		
	def updatePercentage(self):
		self.percent = float(self.hits)/float(self.atBats)
		self.percent = format(self.percent, '.3f')

		
	
#checks for correct # of arguments when executing the script		
if len(sys.argv) < 2 or len(sys.argv)>2:
	sys.exit("Usage: %s filename" %sys.argv[0])

filename = sys.argv[1];
#checks if filename exists in the directory
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

#opens file and reads in the data
infile = open(filename, 'r')
data = infile.read()
infile.close()

#splits up data by lines
dataSplit = data.split('\n')
fileSize=len(dataSplit)

#runs the function on each line of the input file
for i in range(fileSize):
	checkLine(dataSplit[i])

#prints out the cards players in the correct order, sorted by batting average
printSorted(cardsPlayers)